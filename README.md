Timestamp
=============

Object timestamp logic: creation and update times.

## Install

```
composer require pressop/timestamp
```

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
