<?php

/*
 * This file is part of the pressop/timestamp package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Timestamp\Bridge\Symfony;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class PressopTimestampBundle
 *
 * @author Benjamin Georgeault
 */
class PressopTimestampBundle extends Bundle
{
}
