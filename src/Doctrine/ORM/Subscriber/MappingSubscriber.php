<?php

/*
 * This file is part of the pressop/timestamp package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Timestamp\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Pressop\Component\Timestamp\Model\TimestampInterface;

/**
 * Class MappingSubscriber
 *
 * @author Benjamin Georgeault
 */
class MappingSubscriber implements EventSubscriber
{
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(TimestampInterface::class)) {
            $this->mappingTimestamp($metadata);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    private function mappingTimestamp(ClassMetadata $metadata)
    {
        if (!$metadata->hasField('createdAt')) {
            $metadata->mapField([
                'fieldName' => 'createdAt',
                'type' => 'datetime',
            ]);
        }

        if (!$metadata->hasField('updatedAt')) {
            $metadata->mapField([
                'fieldName' => 'updatedAt',
                'type' => 'datetime',
                'nullable' => true,
            ]);
        }
    }
}
