<?php

/*
 * This file is part of the pressop/timestamp package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Timestamp\Model;

/**
 * Interface TimestampInterface
 *
 * @author Benjamin Georgeault
 */
interface TimestampInterface
{
    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime;

    /**
     * @param \DateTime|null $createdAt
     * @return $this
     */
    public function setCreatedAt(?\DateTime $createdAt);

    /**
     * @return null|\DateTime
     */
    public function getUpdatedAt(): ?\DateTime;

    /**
     * @param \DateTime|null $updatedAt
     * @return $this
     */
    public function setUpdatedAt(?\DateTime $updatedAt);
}
