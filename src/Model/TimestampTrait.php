<?php

/*
 * This file is part of the pressop/timestamp package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pressop\Component\Timestamp\Model;

/**
 * Trait TimestampTrait
 *
 * @author Benjamin Georgeault
 * @see TimestampInterface
 */
trait TimestampTrait // implements TimestampInterface
{
    /**
     * @var \DateTime|null
     */
    protected $createdAt;

    /**
     * @var \DateTime|null
     */
    protected $updatedAt;

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt ? : $this->createdAt = new \DateTime();
    }

    /**
     * @param \DateTime|null $createdAt
     * @return $this
     */
    public function setCreatedAt(?\DateTime $createdAt)
    {
        if (null !== $createdAt) {
            $this->createdAt = $createdAt;
        }

        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|null $updatedAt
     * @return $this
     */
    public function setUpdatedAt(?\DateTime $updatedAt)
    {
        if (null !== $updatedAt) {
            $this->updatedAt = $updatedAt;
        }

        return $this;
    }
}
